# DNS-Server configuration :electric_plug: 

### Auftrag 

Wir erhielten den Auftrag, einen DNS-Server zu konfigurieren. Im Unterricht hatten wir den DNS-Server kennengelernt und hatten auch kleine Aufgaben dazu gemacht.

### Configuration 

1. Als erstes ladet man den Windows Server runter. Dabei spielte es keine Rolle ob man 2019 oder 2020 auswählt. Ich hatte die 2019 in Englisch Version ausgewählt.

![Bild16](./Images/bild16.png)

2. Man erstellt eine neue VM und fügt diesen Windows Server hinzu. Während der installation fragte die VM nach den Windows-Schlüssel. Bei der Installation hat man den Schlüssel dazu bekommen. Mir ist dabei aufgefallen, dass es nicht nötig war den Schlüssel einzugeben. 


![Bild17](./Images/bild17.png)

3. Nach dem Aufstarten der Vm und der konfiguration von den ersten Einstellungen, fängt man an den DNS zu installieren und einzurichten. 

4. Damit man einen DNS-Server hinzufügen kann, drückt man ***Add roles and features***. Jetzt folgen sämtliche Schritte zur Installation des Servers.

![Bild22](./Images/bild22.png)

5. In diesem Schritt ist es sehr wichtig, dass man den ***DNS Server*** auswählt, da man auch einen DNS-Server konfiguriert und nicht etwas anderes. 

![Bild19](./Images/bild19.png)

6. Jetzt kann man eine neue Zone hinzufügen, damit man einen ***Zone name*** erstellen kann. Ich habe mich für meinen Nachnamne.ch entschieden (***ferrari.ch***) Ich habe mich dafür entschieden, weil es gut passt und nicht kompliziert ist. 

![Bild20](./Images/bild20.png)

7. Wir sind jetzt so weit gekommen, dass man die ***Network ID*** eingeben muss. 
- Meine Network ID ist **192.100.0.0** Ich habe mich für diese entschieden, da ich es mit der ***SOTA*** in Verbindung setzen wollte. 

![Bild23](./Images/bild23.png)

8. Mit einem rechts Klick auf der Maus, erstelle ich jetzt einen neuen Host und kann es wie ich möchte einstellen. Weiter unten werde ich es Ihnen genauer darstellen.

![Bild24](./Images/bild24.png)

Beim ***Name*** habe ich die Zone Name eingeben (ferrari.ch). Wichtig ist, dass man Web noch vorhin schreibt -->Web.ferrari.ch

Im Kästchen ***IP adress*** habe ich die IP von meinem DNS-Server angegeben (***192.168.1.4***). Somit ist der DNS "verlinkt"

Zum Schluss habe ich auf ***Add Host*** gedrückt.

![Bild25](./Images/bild25.png)

Hier habe ich ein Bild hinzugefügt, wo man sehen kann welche IP-Adresse der DNS-Server von mir bekommen hat. Dies ist sehr wichtig, da sonst der DNS nicht funktionieren wird. 

![Bild26](./Images/bild26.png)

## Ergebnis

Hier habe ich einen Test gemacht, um zu sehen ob die Konfigurationen richtig gemacht wurden. Ich habe einen lookup auf meinen DNS Server gemacht, damit ich sehe ob es eine Verbindung aufbauen kann. Man sieht die Adresse, die ich dem DNS-Server während der Konfiguration zugeteilt habe. 

![Bild21](./Images/bild21.png)

Somit ist der DNS Server fertig eingerichtet und ist einsatzbereit.

### Problem

Mein Problem war, dass mein DNS-Server nach der Konfiguration in der PowerShell ein Time-out bekommen hat, aber trotzdem meinen Client gefunden. Ich wusste nicht, woran es legen konnte. Ich überlegte mir, dass wenn er mit meinem Client kommunizieren möchte, braucht er dann eine Verbindung. Mir ist aufgefallen, dass ich mit dem Internet noch gar nicht verbunden war, also habe ich ein neues hinzugefügt und mich mit dem verbunden, und es hat dann funktioniert.

### Reflexion     :loudspeaker:
Diese Arbeit hat mir viel Freude gemacht, weil sie immer gut funktioniert hat und ich nur ein kleines Problem hatte, das ich schnell lösen konnte. Ich war sehr zufrieden. Ich hatte mich im Internet schlau gemacht und hatte nach einer sinnvollen Anleitung für das Konfigurieren eines DNS-Servers gesucht. Ich hatte am Anfang YouTube-Videos geschaut, leider aber nichts gefunden. Die Videos, die ich sah, waren nicht mit meiner Aufgabe überein. Ich habe schliesslich eine Website gefunden, auf der es Schritt für Schritt erklärt und veranschaulicht wird. Ich konnte wirklich mit dieser Anleitung den DNS-Server ohne Mühe konfigurieren. 

https://computingforgeeks.com/configure-dns-server-on-windows-server-2022/
