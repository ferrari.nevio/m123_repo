

# Filius Auftrag :loudspeaker:

##### Einleitung
Ich habe diesen Auftrag Schritt für Schritt gemacht und werde es hier beschreiben und Dokumentieren. 

##### Einzelne Schritte
Ich habe zuerst die Aufgabenstellung studiert und habe mir einen Überblich verschaffen. 

![Bild1](./Images/bild1.png)

Als aller erstes musste ich im Filius alle Clients und Server / Router miteinander verbunden. Man musste sie verbinden, damit sie alle gegenseitig sich sehen können und kommunizieren können.

![Bild1](./Images/bild5.png)

Folglich habe ich alle IP-Adressen und die Namen entsprechend dieses Bildes konfiguriert. Diese Abbildung ist sehr übersichtlich und hat mir sehr gut geholfen um alles richtig und sorgfälltig zu konfigurieren. 

![Bild1](./Images/bild2.png)

Jetzt habe ich den DHCP Server richtig eingestellt, damit er die IP-Adressen den Clients richtig zuteilt. Ich habe alle MAC-Adressen kopiert und dann eingefügt. Die IP-Adresse habe ich von 10.0.0.5 bis 10.0.0.10 den Clients zugewiesen.

![Bild1](./Images/bild3.png)

## Test:electric_plug: 
Zu letzt habe einen Test gemacht, um zu sehen, dass ich alles richtig konfiguriert habe. Ich habe vom PC21 auf den PC51 gepinnt, weil somit es ein grosser Weg ist. Wenn es wie hier im Bild zu sehen ist kein Timeout kommt, dann ist es erfolgreich rausgekommen und man hat alles richtig eingestellt. 

![Bild1](./Images/bild4.png)