[TOC]

# Inhaltsverzeichnis :open_book:

- [DHCP Configuration](#dhcp-configuration)

- [DHCP mit Cisco Packet Tracer](#dhcp-configuration)

- [Aufgabe 3](#dhcp-configuration)

- [LAB](#dhcp-configuration)




# DHCP Configuration :desktop_computer: 


![Bild1](./Images/bild4.png)
### **Auftrag** 
In diesem Auftrag muss man einen DHCP-Server aufsetzt und konfigurieren.
Der Auftrag muss in Filius bearbeitet werden und in diesem Dokument dokumentiert werden. 


### **Erklärung** 
Das ist ein Netzwerk. Damit man die IP-Adressen Vergeben kann muss man auf dem DHCP-Server draufdrücken. Darauf hin öffnet sich ein Fenster, wo man dann die Konfigurationen machen kann.


### **Einzelne Schritte** 
##### Schritt 1
Als erstes geht man auf Clients und wählt es aus. Folglich wählt man den Hacken **„DHCP nur Konfiguration verwenden“** damit der DHCP den Client annimmt.
![Bild6](./Images/Bild6.png)
##### Schritt 2
Hier kann man die entsprechende Reichweite eingeben.
Bei der Adress-Untergrenze ist die tiefste IP-Adresse und bei der Adress-Obergrenze gibt man die höchste IP an.
![Bild5](./Images/bild5.png)


##### Schritt 3


Hier gibt man die IP- und MAC Adresse eines Clients ein und drückt 
Auf **„hinzufügen“**. Somit ist er hinzugefügt worden. 
![Bild1](./Images/bild1.png)


### *Fazit* 	:checkered_flag:
Ich fand diese Arbeit sehr toll, da ich vieles neues über den DHCP-Server gelernt habe. Ich konnte mein Wissen vergrössern, dank dieser Aufgabe. Ich weiss, wie man einen DHCP-Server konfiguriert und kenne die Zusammenhänge von einem DHCP-Server und einem Client. Die Dokumentation auf zwei Varianten zu schreiben war auch etwas Neues, da ich gewohnt bin, es mit Word zu machen. Mit Visual Studio Code habe ich es noch nie gemacht. Ich hatte am Anfang Mühe mit den Codes, doch wenn man es weiss, wie es geht, macht es gleich viel mehr Freude, daran zu arbeiten. Mir hat der Auftrag sehr gefallen und hat mir Spass gemacht.


## Welche Eigenschaften von einem DHCP-Server muss man kennen? 

Automatische IP-Zuweisung: DHCP-Server ermöglichen die automatische Vergabe von IP-Adressen an Geräte in einem Netzwerk, was die manuelle Konfiguration vereinfacht.

**Netzwerkkonfiguration:** Neben IP-Adressen weist ein DHCP-Server auch Subnetzmasken, Standard -Gateways und DNS-Server zu, um eine vollständige Netzwerkkonfiguration bereitzustellen.

**Lease-Zeiten:** DHCP vergibt temporäre IP-Adressen für einen bestimmten Zeitraum (Lease-Zeit). Geräte müssen ihre Lease erneuern, wenn sie weiterhin im Netzwerk aktiv sein wollen.

**IP-Adresspools:** DHCP-Server verwalten einen Pool von verfügbaren IP-Adressen. Diese Adressen werden den Geräten dynamisch zugewiesen und können nach Ablauf der Lease-Zeit wieder freigegeben werden.

**Konflikterkennung:** DHCP-Server überwachen das Netzwerk auf mögliche IP-Adresskonflikte, um sicherzustellen, dass jedem Gerät eine eindeutige Adresse zugewiesen wird.

**DHCP-Relay:** In grossen Netzwerken kann ein DHCP-Relay verwendet werden, um DHCP-Anfragen über verschiedene Subnetze hinweg weiterzuleiten.

**Sicherheit:** Um Missbrauch zu verhindern, sollte der Zugriff auf den DHCP-Server durch geeignete Sicherheitsmassnahmen geschützt werden.

**Zuverlässigkeit:** Ein zuverlässiger DHCP-Server ist entscheidend für die kontinuierliche Konnektivität im Netzwerk, Backup-Server können eingerichtet werden, um Ausfallzeiten zu minimieren.


 ## Prüfungsvorbereitung :heavy_exclamation_mark:

Option 61: MAC-Adresse

Option 53: DHCP Message Typen

### Redundanz bei einem Server 
Ein redundanter Server ist ein Server, auf dem aktuell keine Kameradaten verarbeitet werden. Bei Ausfall eines Servers übernimmt ein redundanter Server dessen gesamte Kamera- und Geräteverarbeitung. Das bedeutet, dass alle Kameras auf den redundanten Server verschoben und von diesem verarbeitet werden.

- mehr Leistung
- höhere Sicherheit 
- Redundanz

Server (Port 67)

Client (Port 68) 

**Dora** = Discover, Offer, Request, acknowledge

 **Grün**= ARP Pakete(Layer 2) Vermittlung, überprüft die Verbindung von der IP-Adresse und Mac-Adresse

  **Blau**= normale Pakete(Layer 3-7) Anwendung


# DHCP /Cisco Packet Tracer :desktop_computer: 

### **Auftrag 2**



**Geräte mit fixen IP-Adressen**

- Drucker

- Access point

- Server (Dienstleistungnen)

- DNS 

- Router

- Layer 3 Switch
### 1. Router-Konfiguration auslesen 
***Für welches Subnetz ist der DHCP Server aktiv?***

 - 192.168.33.1

 ***Welche IP-Adressen ist vergeben und an welche MAC-Adressen?***

- 192.168.33.28 /  0007.ECB6.4534  

- 192.168.33.25 /  0001.632C.3508   
                    
- 192.168.33.27 /  00E0.8F4E.65AA   

- 192.168.33.26 /  0050.0F4E.1D82    

- 192.168.33.24 /  0009.7CA6.4CE3   

- 192.168.33.29 / 00D0.BC52.B29BA


***In welchem Range vergibt der DHCP-Server IPv4 Adressen?***

- 192.168.33.1     - 192.168.33.254 


 ***Was hat die Konfiguration ip dhcp excluded-address zur Folge?***

Das sie eine fixe IP-Adresse bekommen.


***Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?***

- 254

## 2. DORA - DHCP Lease beobachten 

***Welcher OP-Code hat der DHCP-Offer?***

- 0x0000000000000002

***Welcher OP-Code hat der DHCP-Request?***

- x0000000000000001

***Welcher OP-Code hat der DHCP-Acknowledge?***

- x0000000000000002

***An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?***

- es wird an die Broadcast gesendet. Dies ist die 255.255.255.255

***An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?***

- FF.FF.FF.FF.FF.FF

***Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?***

- Weil es ein Broadcast ist und der an alle geschickt wird. Der Switch kann damit nichts anfangen und leitet es weiter. 

***Welche IPv4-Adresse wird dem Client zugewiesen?***

- Die erste freie IP-Adresse in diesem range wird an den Client vergeben.

![Bild1](./Images/bild7.png)

bei diesem Bild kann man sehen, was es für ein OP code ist.
![Bild1](./Images/bild8.png)

![Bild1](./Images/bild9.png)
Da kann man gut erkennen, wie die Settings beim Server aussehen und was bei welchem Layer passiert.
![Bild1](./Images/bild10.png)

# Aufgabe 3 :pencil2:
### 3. Netzwerk umkonfigurieren 

![Bild1](./Images/bild12.png)
Ich habe dem Server diese IP-Adresse (192.168.33.30)
![Bild1](./Images/bild13.png)

![Bild1](./Images/bild14.png)

# LAB :pencil2:
### Auftrag 
Wir bekamen den Auftrag, einen DHCP auf einer VM einzurichten. Wir müssen einen Client haben und einen DHCP-Server, der die IP-Adressen verteilt. Wir verbinden diesen Auftrag mit dem Modul M117 (Sota GmbH). 

## Einzlne Schritte

1. Zuerst ladet man den Ubuntu-Server runter.
2. Man öffnet VMWare Workstation und erstellt eine Neue VM, bei der man die ISO Image hinzufügt.
3. Man konfiguriert dem entsprechend den DHCP Server.
4. Damit wir alles richtig aufsetzten konnten, gab es eine Website, wo es die entsprechenden Kommandos gab. Wir konnten uns gut mit den Kommandos durchsetzten, weil wir keine Schwerigkeiten hatten. 

### Dokumentation 	:bookmark_tabs:
Ich hatte diesen Auftrag mit Goncalo zusammen gemacht, weil es bei mir sehr langsam war und ich nicht arbeiten konnte. Wir konnten zusammen sehr schnell Arbeiten und sind gut vorangekommen. 

Hier habe ich ein Bild 

![Bild1](./Images/bild15.png)

Bei der zweiten Netzwerkkarte (internes Network) hat uns Herr Calisto geholfen, weil wir bei einem Schritt hängengeblieben sind. Die Hilfe hat uns sehr viel gebracht und konnten somit den DHCP-Server weiter konfigurieren. Wir hatten uns gegenseitig gut geholfen. 
Wichtig war das man den NAT-Adapter . Damit man diesen auslesen kann, muss man ***Ip A*** eingeben. Mit diesem Kommando sieht man gleich zwei. Wir brauchen nur die zweite Schnittstelle, weil man mit dieser den NAT einrichten kann, da der DHCP-Server IP-Adressen automatisch zuteilen kann. 


Der DHCP-Server bekam die IP-Adresse ***192.168.100.1***

Unser NAT-Adapter ***ens256***

### Wichtige Daten 

- Ip a

- ens256

- 192.168.100.1


### Unser Problem :heavy_exclamation_mark:

Der Auftrag bestand darin, dass der DHCP eine IP-Adresse an unseren Client sendet. Wir konnten den DHCP erfolgreich konfigurieren und aufsetzen. Unser Problem war, dass wir nicht wussten, was wir bei unserem Kunden machen sollten, damit sie zusammen kommunizieren können. 
