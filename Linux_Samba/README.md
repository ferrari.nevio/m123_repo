

## Samba installation auf Linux :loudspeaker:

#### Auftrag
Wir bekamen den Auftrag SambaShare auf Linux zu installieren und einen Ordner mit dem Host teilen zu können. 

#### Einführung

Herr Da Silva und ich hatten uns zuerst eine passende Anleitung rausgesucht, die für unsere Version passt. Herr Da Silva hat für mich diesen Part übernohmen und hat eine sehr gute Anleitung gefunden, die uns zum Erfolg gebracht hat. 

#### Konfiguration :electric_plug: 
 
 Als erstes haben wir den Ubuntu Destktop heruntergeladen und anschliessend in die VMware eingefügt. Die nächsten Konfigurtionsschritte sind die üblichen, wenn man ein neues Gerät aufsetzt.

1. Sobald es fertig aufgesetzt ist, geht man in den Terminal und gibt das erste Kommando ein. Mit diesem Kommando macht man alle Systemupdates.

![Bild1](./Images/bild1.png)

2. Hiermit überprüfen wir, ob der Dienst zur Verfügung steht und bereit ist.

![Bild2](./Images/bild2.png)

3. Somit bestätigen wir im System die Installation und es wird heruntergeladen.

![Bild3](./Images/bild3.png)

4. Wenn nach diesem Befehl eine grüne Bestätigung erscheint, dann hat die Installation erfolgreich stattgefunden. 

![Bild4](./Images/bild4.png)  
 
 5. Nun erlauben Sie Samba den Firewall zugrief auf Ubuntu. Um über das SMB-Protokoll eine Verbindung herzustellen und auf die freigegebenen Dateien zuzugreifen, haben wir zunächst eine Whitelist erstellt und den Zugriff auf den Dienst in der Firewall von außerhalb des Computers zugelasst.

![Bild5](./Images/bild5.png)  

6. Nun haben wir mit diesem Kommando einen User erstellt und in den nächsten Schritten ein passendes Passwort generiert.
 
![Bild6](./Images/bild6.png) 

7. Wir sind nach diesen Schritten im Client eingeloggt und können mit dem Auftrag anfangen. Wir haben einen neuen Ordner erstellt und ihn nach **"Samba"** genannt. 

![Bild7](./Images/bild7.png) 

8. Damit wir dann auf diesen Ordner zugreifen können, muss man im Lokalem Netzwerk diesen freigeben. Wichtig dabei ist, dass man den zweiten Haken auswählt, weil sonst die anderen (Client) nicht darauf zugreifen können.

![Bild8](./Images/bild8.png) 

9. Jetzt gibt man diesen Ordner im Lokalem Netwerk frei. Man drückt aufs **"plus"** und gibt dann smb://ip-adresse/name-des-freigegebenen-ordners und klickt dann auf verbinden.

![Bild9](./Images/bild9.png) 

10.  Zum Schluss haben wir auf den Client gewechselt und haben uns mit dem lokalen Netzwerk verbunden. Man geht auf den Explorer und macht einen Rechtsklick auf #**"Dieser PC"** und drückt auf **"Netzwerkadresse hinzufügen"**.

![Bild10](./Images/bild10.png) 

11.  Als letztes haben wir wie im Screenshot zu sehen diese URL eingegeben, damit die interne Verbindung zur Verfügung steht. 

![Bild11](./Images/bild11.png)

### Reflexion
Dieser Auftrag hat uns beiden sehr viel Spass gemacht und wir waren sehr gut im Team zusammen. Uns ist es sehr gut gelungen, da wir nie Schwierigkeiten hatten und uns immer gegenseitig geholfen haben. Ich bin sehr stolz auf uns beide, weil wir als erstes Fehlerfrei diesen Auftrag abgegeben haben. Wir sind sehr froh, dass wir eine gute Note haben.